%%-------------------------------------------------------------------
affiche([]).
affiche([A|B]) :-  affiche(B), write(A) ,nl.

%%-------------------------------------------------------------------
afficheInv([]).
afficheInv([A|B]) :- afficheInv(B) , write(A) ,nl .

%%-------------------------------------------------------------------
premier([P|_],P).
%%-------------------------------------------------------------------
premierAff([P|_]) :- write(P) , nl;

%%-------------------------------------------------------------------

dernier([D],D).
dernier([_|R],D) :- dernier(R,D).

%%-------------------------------------------------------------------
dernierAff([D]) :- write(D) ,nl.
dernierAff([_|D]):- dernierAff(D).

%%-------------------------------------------------------------------
element([E|_],E).
element([_|R],E) :- element(R,E).
%%-------------------------------------------------------------------
notelement([],_).
notelement([A|R],B) :- A \= B , notelement(R,B).
%%-------------------------------------------------------------------

compte([],0).
compte([_|L],N) :- compte(L,N1) , N is N1+1 ,N > 0.

%%-------------------------------------------------------------------
somme([],0).
somme([A|R],S) :- somme(R,S1) , S is S1+A .

%%-------------------------------------------------------------------

nieme(1,[A|_],A).
nieme(N,[_|R],X) :- nieme(N1,R,X) , N is N1+1.

%%-------------------------------------------------------------------

occurrence([],_,0).
occurrence([X|L],X,N) :- occurrence(L,X,N1) , N is N1+1.
occurrence([A|L],X,N) :- A \== X ,occurrence(L,X,N).

%%-------------------------------------------------------------------
sous-ens([],_).
sous-ens([X|R],L2) :- element(X,L2) , sous-ens(R,L2).

%%-------------------------------------------------------------------
concat(L1,[],L1).
concat([],L2,L2).
concat([A1|R1],[B1|R2],[A1,B1|R]) :- concat(R1,R2,R).

%%-------------------------------------------------------------------
troisieme(E,[_,_,E|A]).

%%-------------------------------------------------------------------
avder(E,[E,_]).
avder(E,[_|R]) :- avder(E,R).
%%-------------------------------------------------------------------
tri([]).
tri([_,_,_|R]) :- tri(R).

%%-------------------------------------------------------------------
elementpair([_],[]).
elementpair([_,E|R],[E|L]) :- elementpair(R,L).
%%-------------------------------------------------------------------
impair([],[]).
impair([A],[A]).
impair([A,_|R],[A|L]) :- impair(R,L).

%%-------------------------------------------------------------------
listePos([A|_],1,A) :- !.
listePos([_|R],P,X) :- listePos(R,P1,X) , P is P1+1.

%%-------------------------------------------------------------------
inverse([],[]).
inverse([E|R],L) :- append(L1,[E],L) , inverse(R,L1) .

%%-------------------------------------------------------------------
max([A],A).
max([A|R],M) :- max(R,A) , A > M.
max([A|R],M) :- max(R,M) , M >= A.
